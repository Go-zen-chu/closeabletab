﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace CloseableTab
{
    public class CloseableTabItem : TabItem
    {
        CloseableHeader closeableTabHeader;

        public string Title
        {
            set
            {
                ((CloseableHeader)this.Header).tabTitleLabel.Content = value;
            }
        }

        public CloseableTabItem()
        {
            // Create an instance of the usercontrol
            closeableTabHeader = new CloseableHeader();
            // Assign the usercontrol to the tab header
            this.Header = closeableTabHeader;
            closeableTabHeader.closeButton.MouseEnter += closeButton_MouseEnter;
            closeableTabHeader.closeButton.MouseLeave += closeButton_MouseLeave;
            closeableTabHeader.closeButton.Click += closeButton_Click;
        }

        protected override void OnSelected(System.Windows.RoutedEventArgs e)
        {
            base.OnSelected(e);
            ((CloseableHeader)this.Header).closeButton.Visibility = System.Windows.Visibility.Visible;
        }

        protected override void OnUnselected(System.Windows.RoutedEventArgs e)
        {
            base.OnUnselected(e);
            ((CloseableHeader)this.Header).closeButton.Visibility = System.Windows.Visibility.Hidden;            
        }

        protected override void OnMouseEnter(System.Windows.Input.MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            ((CloseableHeader)this.Header).closeButton.Visibility = System.Windows.Visibility.Visible;
        }

        protected override void OnMouseLeave(System.Windows.Input.MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            ((CloseableHeader)this.Header).closeButton.Visibility = System.Windows.Visibility.Hidden;
        }


        // Button MouseEnter - When the mouse is over the button - change color to Red
        void closeButton_MouseEnter(object sender, MouseEventArgs e)
        {
            ((CloseableHeader)this.Header).closeButton.Foreground = Brushes.Red;
        }
        // Button MouseLeave - When mouse is no longer over button - change color back to black
        void closeButton_MouseLeave(object sender, MouseEventArgs e)
        {
            ((CloseableHeader)this.Header).closeButton.Foreground = Brushes.Black;
        }
        // Button Close Click - Remove the Tab - (or raise
        // an event indicating a "CloseTab" event has occurred)
        void closeButton_Click(object sender, RoutedEventArgs e)
        {
            ((TabControl)this.Parent).Items.Remove(this);
        }
    }
}
